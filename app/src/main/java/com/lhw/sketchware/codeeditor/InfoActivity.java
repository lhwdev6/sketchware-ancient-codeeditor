package com.lhw.sketchware.codeeditor;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.net.*;
import android.text.*;
import android.util.*;
import android.webkit.*;
import android.animation.*;
import android.view.animation.*;
import java.util.*;
import java.text.*;
import android.support.v7.app.AppCompatActivity;
import java.util.HashMap;
import java.util.ArrayList;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ChildEventListener;

public class InfoActivity extends AppCompatActivity {
	
	private FirebaseDatabase _firebase = FirebaseDatabase.getInstance();
	
	private HashMap<String, Object> licenseLatest = new HashMap<>();
	
	private ArrayList<HashMap<String, Object>> licenseVersions = new ArrayList<>();
	
	private LinearLayout lay;
	private LinearLayout linear;
	private ImageView imageview1;
	private TextView textview1;
	private TextView textview2;
	private TextView details;
	
	private DatabaseReference licenseFirebase = _firebase.getReference("licenses");
	private ChildEventListener _licenseFirebase_child_listener;
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.info);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		
		lay = (LinearLayout) findViewById(R.id.lay);
		linear = (LinearLayout) findViewById(R.id.linear);
		imageview1 = (ImageView) findViewById(R.id.imageview1);
		textview1 = (TextView) findViewById(R.id.textview1);
		textview2 = (TextView) findViewById(R.id.textview2);
		details = (TextView) findViewById(R.id.details);
		
		_licenseFirebase_child_listener = new ChildEventListener() {
			@Override
			public void onChildAdded(DataSnapshot _param1, String _param2) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onChildChanged(DataSnapshot _param1, String _param2) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onChildMoved(DataSnapshot _param1, String _param2) {
				
			}
			
			@Override
			public void onChildRemoved(DataSnapshot _param1) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onCancelled(DatabaseError _param1) {
				final int _errorCode = _param1.getCode();
				final String _errorMessage = _param1.getMessage();
				
			}
		};
		licenseFirebase.addChildEventListener(_licenseFirebase_child_listener);
	}
	private void initializeLogic() {
		String content = getIntent().getStringExtra("view");
		textview1.setTextSize(27);
		if(content != null){
			if(content.equals("openSource")){
				setTitle("Open source licenses");
				final TextView t = new TextView(this);
				t.setPadding(24, 24, 24, 24);
				t.setTextColor(0xff444444);
				t.setTypeface(Typeface.MONOSPACE);
				t.setAutoLinkMask(android.text.util.Linkify.WEB_URLS);
				
				setContentView(t);
				
				licenseFirebase.addListenerForSingleValueEvent(new ValueEventListener() {
					@Override
					public void onDataChange(DataSnapshot _dataSnapshot) {
						licenseVersions = new ArrayList<>();
						try {
							GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
							for (DataSnapshot _data : _dataSnapshot.getChildren()) {
								HashMap<String, Object> _map = _data.getValue(_ind);
								licenseVersions.add(_map);
							}
						}
						catch (Exception _e) {
							_e.printStackTrace();
						}
						if (licenseVersions.size() == 0) {
							throw new RuntimeException("firebase DB is wrong: InfoActivity/licenseView @root/licenses has no child");
						}
						licenseLatest = licenseVersions.get((int)licenseVersions.size() - 1);
						t.setText((CharSequence) licenseLatest.get("text"));
					}
					@Override
					public void onCancelled(DatabaseError _databaseError) {
					}
				});
			}} else {
			String version = ""; try {
				android.content.pm.PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
				version = info.versionName;
			}catch(Exception e){}
			final View lay = findViewById(R.id.lay);
			getActionBar().hide();
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
			if(apiLevel >= 21){
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				lay.setElevation(9);
				lay.setBackground(new android.graphics.drawable.RippleDrawable(android.content.res.ColorStateList.valueOf(0x33000000),  new android.graphics.drawable.GradientDrawable(android.graphics.drawable.GradientDrawable.Orientation.BR_TL, new int[]{/*0xff455a64*/0xffaa305a, 0xff455a64}), null));
				lay.setOnClickListener(new View.OnClickListener(){
									@Override
									public void onClick(View v)
									{
										_startEasterEgg();
									}
							}); textview1.setText("CodeEditor"); textview2.setText("by LHW");
			} else { //when api level is lower than 21
				lay.setBackground(new android.graphics.drawable.GradientDrawable(android.graphics.drawable.GradientDrawable.Orientation.BR_TL, new int[]{/*0xff455a64*/0xffaa305a, 0xff455a64}));
			}
			linear.addView(makeCard(R.drawable.ic_cloud_queue_grey, "My github", GITHUB, new View.OnClickListener(){public void onClick(View view){
					Intent i = new Intent();
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); i.setAction(Intent.ACTION_VIEW);
					i.setData(Uri.parse(GITHUB));
					startActivity(i);
				}}));
			linear.addView(makeCard(R.drawable.ic_mail_grey, "My email", EMAIL, new View.OnClickListener(){public void onClick(View v){
					Intent i = new Intent();
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					i.setAction(Intent.ACTION_VIEW); 
					i.setData(Uri.parse(EMAIL));
					startActivity(i);
				}
			}));
			linear.addView(makeCard(R.drawable.ic_copyright_gray, "Open source license", null, new View.OnClickListener(){public void onClick(View view){Intent intent = new Intent(InfoActivity.this, InfoActivity.class); intent.putExtra("view", "openSource");
					startActivity(intent);}}));
		}}
	private static final String GITHUB = "https://gusdn2.github.io/";
	private static final String EMAIL = "lhwdev6@gmail.com";
	
	private LinearLayout makeCard(int icon, CharSequence _title, CharSequence _subtitle, View.OnClickListener listener){
		LinearLayout lay1 = new LinearLayout(this);
		if(listener != null) lay1.setOnClickListener(listener);
		lay1.setPadding(18, 18, 18, 18);
		ImageView img1 = new ImageView(this);
		img1.setPadding(8, 8, 8, 8);
		img1.setImageResource(icon);
		lay1.addView(img1);
		LinearLayout lay2 = new LinearLayout(this);
		lay2.setOrientation(LinearLayout.VERTICAL);
		lay2.setPadding(12, 8, 8, 8);
		TextView title = new TextView(this);
		title.setText(_title);
		title.setTextColor(0xff333333);
		title.setTextSize(16);
		lay2.addView(title);
		if(_subtitle != null){
			TextView subtitle = new TextView(this);
			subtitle.setText(_subtitle);
			subtitle.setTextSize(12);
			subtitle.setTextColor(0xff888888);
			lay2.addView(subtitle);
		}
		lay1.addView(lay2);
		android.graphics.drawable.GradientDrawable g = new android.graphics.drawable.GradientDrawable();
		g.setColor(0xffffffff);
		g.setCornerRadius(3);
		if(apiLevel < 21)
		lay1.setBackground(g);
		else {
			lay1.setElevation(2);
			lay1.setBackground(new android.graphics.drawable.RippleDrawable(android.content.res.ColorStateList.valueOf(0x44000000), g, null));
		}
		lay1.setGravity(Gravity.CENTER_VERTICAL);
		ViewGroup.MarginLayoutParams mlp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		mlp.setMargins(8, 8, 64, 8);
		lay1.setLayoutParams(mlp);
		return lay1;
	}
	
	@Override
	protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
		super.onActivityResult(_requestCode, _resultCode, _data);
		
		switch (_requestCode) {
			
			default:
			break;
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		isCut = true;
	}
	private void _startEasterEgg () {
		lay.removeAllViews();
		int[] icons = {
			R.drawable.thumb_main,
			R.drawable.thumb_gray,
			R.drawable.thumb_white,
			R.drawable.thumb_pink,
			R.drawable.thumb_orange,
			R.drawable.thumb_skyblue,
			R.drawable.thumb_bluegreen,
			R.drawable.thumb_indigo,
			R.drawable.thumb_black
		};
		final int[] barrierResources = {R.drawable.ic_barrier1, R.drawable.ic_barrier2, R.drawable.ic_barrier3, R.drawable.ic_barrier4, R.drawable.ic_barrier5};
		final int layw = lay.getWidth();
		final int layh = lay.getHeight();
		final ImageView img = new ImageView(this);
		img.setImageResource(icons[new Random().nextInt(icons.length)]);
		final MyLayout f = new MyLayout(this);
		f.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));lay.removeView(img);
		lay.addView(f);
		f.addView(img);
		img.setX((f.getWidth() - img.getWidth()) / 2);
		img.setY((f.getHeight() - img.getHeight()) / 2);
		final ArrayList<Float> positions = new ArrayList<Float>();
		final ArrayList<ImageView> barriers = new ArrayList<ImageView>();
		
		update = new Runnable(){
			public long startTime = -1;
			public long lastTime;
			public float dt;
			public float fast = 1f;
			public float createDelay = 0f;
			public Handler handler= new Handler();
			public Random random = new Random();
			public int temp = 0;
			public void run(){
				if(startTime == -1){
					//init
					startTime = System.currentTimeMillis();
				}
				dt = (float)(System.currentTimeMillis() - lastTime) / 1000;
				lastTime = System.currentTimeMillis();
				//game main
				temp = (temp + 1) % 360;
				img.setRotation(temp);
				createDelay += dt;
				if(createDelay >= 1f && random.nextFloat() % 4f < createDelay){
					//create barrier
					createDelay = 0;
					ImageView barrier = new ImageView(InfoActivity.this);
					barrier.setImageResource(barrierResources[random.nextInt(barrierResources.length)]);
					barriers.add(barrier);
					positions.add((float) lay.getWidth());
					lay.addView(barrier);
					barrier.setX(0);
					barrier.setY(lay.getHeight() - barrier.getHeight() - 10);
				}
				for(int i = 0; i < barriers.size(); i++){ ImageView img = barriers.get(i);
					float x = positions.get(i);
					if(x < 0){
						   lay.removeView(barriers.get(i));
						   barriers.remove(i);
						   positions.remove(i);
					} else {
						   x -= dt * lay.getWidth() / 4;
						   img.setX((int) x);
						   positions.set(i, x);
					}
				}
				if(isCut) return;
				
				float delay;
				float fps = getFps();
				if(fps > 30f){ fps = 40f; delay = 1f / 40f - dt; }
				else delay = 0f;
				handler.postDelayed(this, (int)(delay * 1000));
			}
			public float getFps(){
				return 1f / (float) dt;
			}
			public void stop(){
				isCut = true;
				handler.removeCallbacks(this);
			}
		};
		update.run();
	}
	private Runnable update;
	private boolean isCut = false;
	
	public class MyLayout extends ViewGroup
		{
				public MyLayout(Context context)
				{
						super(context);
				}
				
				@Override
				protected void onLayout(boolean updated, int l, int t, int r, int d)
				{
						int count = getChildCount();
						
						for(int i = 0; i < count; i++){
								View child = getChildAt(i);
								child.layout((int) child.getX(), (int) child.getY(), (int) child.getX() + child.getWidth(), (int) child.getY() + child.getHeight());
						}
		}
	}
	
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input){
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels(){
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels(){
		return getResources().getDisplayMetrics().heightPixels;
	}
	
}
