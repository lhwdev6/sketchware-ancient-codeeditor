package com.lhw.sketchware.codeeditor;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.net.*;
import android.text.*;
import android.util.*;
import android.webkit.*;
import android.animation.*;
import android.view.animation.*;
import java.util.*;
import java.text.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import java.util.ArrayList;
import java.util.HashMap;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.Intent;
import android.net.Uri;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class SettingActivity extends AppCompatActivity {
	
	
	private Toolbar _toolbar;
	
	private ArrayList<HashMap<String, Object>> menus = new ArrayList<>();
	
	private ListView list;
	
	private SharedPreferences fileSetting;
	private Intent intent = new Intent();
	private SharedPreferences tuto;
	private AlertDialog.Builder dialog;
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.setting);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		
		_toolbar = (Toolbar) findViewById(R.id._toolbar);
		setSupportActionBar(_toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _v) {
				onBackPressed();
			}
		});
		list = (ListView) findViewById(R.id.list);
		fileSetting = getSharedPreferences("setting", Activity.MODE_PRIVATE);
		tuto = getSharedPreferences("tutorial", Activity.MODE_PRIVATE);
		dialog = new AlertDialog.Builder(this);
	}
	private void initializeLogic() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle("Settings");
		_initList();
	}
	private ArrayList<View> itemViews = new ArrayList<View>();
	 @Override public boolean onOptionsItemSelected(MenuItem item){
		if(item.getItemId() == android.R.id.home) finish();
		return true;
	}
	
	@Override
	protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
		super.onActivityResult(_requestCode, _resultCode, _data);
		
		switch (_requestCode) {
			
			default:
			break;
		}
	}
	
	private void _onListClicked (final double _position) {
		HashMap<String, String> options = (HashMap<String, String>)list.getItemAtPosition((int)_position);
		if(options.containsKey("check")){ CheckBox check = (CheckBox) itemViews.get((int)_position).findViewById(R.id.check);
			check.setChecked(!check.isChecked());
		}
		if (_position == 0) {
			LinearLayout lay = new LinearLayout(this);
			lay.setOrientation(LinearLayout.VERTICAL);
			final RadioButton b1 = new RadioButton(this);
			
		}
		else {
			if (_position == 1) {
				intent.setAction(Intent.ACTION_VIEW);
				intent.setClass(getApplicationContext(), ThemesettingActivity.class);
				startActivity(intent);
			}
			else {
				if (_position == 2) {
					CharSequence[] list = new CharSequence[15];
					for(int i=0; i<=14; i++) list[i] = Integer.toString(i + 10) + "sp";
					new AlertDialog.Builder(this).setTitle("Font size").setSingleChoiceItems(list, Integer.parseInt(fileSetting.getString("textSize", "16")) - 10, new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int witch){ fileSetting.edit().putString("textSize", Integer.toString(witch + 10)).apply(); ((AlertDialog)dlg)
							.hide();
							_initList(); }}).setNegativeButton("Cancel", null).show();
					
				}
				else {
					if (_position == 3) {
						LinearLayout lay = new LinearLayout(this);
						lay.setPadding(45, 30, 45, 30);
						lay.setOrientation(LinearLayout.VERTICAL);
						final CheckBox check = new CheckBox(this);
						check.setText("Enable Auto-Save");
						check.setChecked(fileSetting.getString("autoSaveInterval", "-1") != "-1");
						lay.addView(check);
						final EditText edit = new EditText(this);
						edit.setInputType(InputType.TYPE_CLASS_NUMBER);
						edit.setHint("Auto save interval(min)");
						edit.setText(Double.toString(Integer.parseInt(fileSetting.getString("autoSaveInterval", "0")) / 60000.0));
						edit.setEnabled(check.isChecked());
						lay.addView(edit);
						check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){public void onCheckedChanged(CompoundButton button, boolean checked){ edit.setEnabled(checked); }});
						new AlertDialog.Builder(this)
						.setTitle("Auto Save")
						.setView(lay)
						.setPositiveButton("Save", new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int witch){ if(check.isChecked()) fileSetting.edit().putString("autoSaveInterval", Integer.toString((int)( Double.parseDouble(edit.getText().toString()) * 60000.0))).commit();
								else fileSetting.edit().putString("autoSaveInterval", "-1").commit();
								_initList();
							}})
						.setNegativeButton("Cancel", null)
						.show();
					}
					else {
						if (_position == 4) {
							intent.setAction(Intent.ACTION_VIEW);
							intent.setClass(getApplicationContext(), InfoActivity.class);
							startActivity(intent);
						}
						else {
							if (_position == 5) {
								_developOptions();
							}
						}
					}
				}
			}
		}
	}
	
	
	private void _developOptions () {
		if ("".equals(tuto.getString("develop_warn", ""))) {
			dialog.setTitle("Warning");
			dialog.setMessage("Do not use develop options when it is not necessary.\nIt might can make CodeEditor is cannot restart.");
			dialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					tuto.edit().putString("develop_warn", "true").commit();
				}
			});
			dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					_initList();
				}
			});
			dialog.setCancelable(false);
			dialog.create().show();
		}
		menus.clear();
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Back");
			menus.add(_item);
		}
		
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Throw Exception");
			menus.add(_item);
		}
		
		menus.get((int)1).put("subtitle", "throw the java.lang.RuntimeException");
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Set key value");
			menus.add(_item);
		}
		
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Read key value");
			menus.add(_item);
		}
		
		list.setOnItemClickListener(new AdapterView.OnItemClickListener(){public void onItemClick(AdapterView av, View v, int pos, long id){_onListClickedDev(pos);}});
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){public boolean onItemLongClick(AdapterView av, View v, int pos, long id){ return false; }});
		list.setAdapter(new ListAdapter(menus));
	}
	
	
	private void _initList () {
		menus.clear();
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "File storage");
			menus.add(_item);
		}
		
		if (fileSetting.getString("storagePrivate", "").equals("true")) {
			menus.get((int)0).put("subtitle", "Files is saved at private dir.");
		}
		else {
			menus.get((int)0).put("subtitle", "Files is saved at public dir.");
		}
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Theme");
			menus.add(_item);
		}
		
		menus.get((int)1).put("subtitle", "Set the theme of codeeditor.");
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Font size");
			menus.add(_item);
		}
		
		menus.get((int)2).put("subtitle", fileSetting.getString("textSize", "").concat("sp"));
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Auto Save");
			menus.add(_item);
		}
		
		menus.get(3).put("subtitle", (fileSetting.getString("autoSaveInterval", "-1") == "-1" ? "disenabled": ("enabled, save interval: " + Integer.parseInt(fileSetting.getString("autoSaveInterval", "0")) / 60000)));
		{
			HashMap<String, Object> _item = new HashMap<>();
			_item.put("title", "Information");
			menus.add(_item);
		}
		
		if(fileSetting.getBoolean("debugEnabled", false)){
			{
				HashMap<String, Object> _item = new HashMap<>();
				_item.put("title", "Developer Options");
				menus.add(_item);
			}
			
		}
		list.setOnItemClickListener(new AdapterView.OnItemClickListener(){public void onItemClick(AdapterView av, View v, int pos, long id){_onListClicked(pos);}});
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){public boolean onItemLongClick(AdapterView av, View v, int pos, long id){if(((TextView)v.findViewById(R.id.title)).getText() == "Information"){
					boolean enabled = fileSetting.getBoolean("debugEnabled", false);
					fileSetting.edit().putBoolean("debugEnabled", !enabled).commit();
					new AlertDialog.Builder(SettingActivity.this).setTitle("Developer Options").setMessage((enabled?"Disenabled Developers Option.":"Enabled Developer Options. Restart app to apply.")).setPositiveButton("Okay", null).show(); return true; } return false; }});
		list.setAdapter(new ListAdapter(menus));
	}
	
	
	private void _onListClickedDev (final double _pos) {
		if (_pos == 0) {
			_initList();
		}
		else {
			if (_pos == 1) {
				LinearLayout lay = new LinearLayout(this);
				final EditText edit = new EditText(this);
				edit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				lay.addView(edit);
				lay.setPadding(45, 0, 45, 30);
				new AlertDialog.Builder(this)
				.setTitle("Throws Exception")
				.setMessage("Set message of exception")
				.setView(lay)
				.setPositiveButton("Throw", new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int witch){throw new RuntimeException(edit.getText().toString());}})
				.setNegativeButton("Cancel", null)
				.show();
			}
			else {
				if (_pos == 2) {
					LinearLayout lay = new LinearLayout(this);
					lay.setOrientation(LinearLayout.VERTICAL);
					final EditText edit = new EditText(this);
					edit.setHint("file");
					edit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
					lay.addView(edit);
					final EditText edit2 = new EditText(this);
					edit2.setHint("key");
					edit2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
					lay.addView(edit2);
					final EditText edit3 = new EditText(this);
					edit3.setHint("value");
					edit3.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
					lay.addView(edit3);
					final EditText edit4 = new EditText(this);
					edit4.setHint("type");
					edit4.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
					lay.addView(edit4);
					lay.setPadding(45, 30, 45, 30);
					new AlertDialog.Builder(this)
					.setTitle("Set key value")
					.setView(lay)
					.setPositiveButton("Okay", new DialogInterface.OnClickListener(){
						public void onClick(DialogInterface dlg, int witch){
							SharedPreferences.Editor sf= getSharedPreferences(edit.getText().toString(), Context.MODE_PRIVATE).edit();
							if(edit4.getText().toString().indexOf("string") != -1) sf.putString(edit2.getText().toString(), edit3.getText().toString());
							else if(edit4.getText().toString().indexOf("number") != -1) sf.putInt(edit2.getText().toString(), Integer.parseInt(edit3.getText().toString()));
							else if(edit4.getText().toString().indexOf("boolean") != -1) sf.putBoolean(edit2.getText().toString(), edit3.getText().equals("true"));
							else showMessage("wrong type");
							sf.commit();
						}
					})
					.setNegativeButton("Cancel", null)
					.show();
				}
				else {
					if (_pos == 3) {
						LinearLayout lay = new LinearLayout(this);
						lay.setOrientation(LinearLayout.VERTICAL);
						final EditText edit = new EditText(this);
						edit.setHint("file");
						edit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
						lay.addView(edit);
						final EditText edit2 = new EditText(this);
						edit2.setHint("key");
						edit2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
						lay.addView(edit2);
						lay.setPadding(45, 30, 45, 30);
						new AlertDialog.Builder(this)
						.setTitle("Read key value")
						.setView(lay)
						.setNegativeButton("Cancel", null)
						.setPositiveButton("Okay", new DialogInterface.OnClickListener(){ public void onClick(DialogInterface _dlg, int witch){
								new AlertDialog.Builder(SettingActivity.this)
								.setTitle("Result")
								.setMessage(SettingActivity.this.getSharedPreferences(edit.getText().toString(), Context.MODE_PRIVATE).getString(edit2.getText().toString(), "CANNOT READ VALUE"))
								.show();
							}})
						.show();
					}
					else {
						
					}
				}
			}
		}
	}
	
	
	public class ListAdapter extends BaseAdapter {
		ArrayList<HashMap<String, Object>> _data;
		public ListAdapter(ArrayList<HashMap<String, Object>> _arr) {
			_data = _arr;
		}
		
		@Override
		public int getCount() {
			return _data.size();
		}
		
		@Override
		public HashMap<String, Object> getItem(int _index) {
			return _data.get(_index);
		}
		
		@Override
		public long getItemId(int _index) {
			return _index;
		}
		@Override
		public View getView(final int _position, View _view, ViewGroup _viewGroup) {
			LayoutInflater _inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View _v = _view;
			if (_v == null) {
				_v = _inflater.inflate(R.layout.listitem, null);
			}
			
			final LinearLayout linear1 = (LinearLayout) _v.findViewById(R.id.linear1);
			final CheckBox check = (CheckBox) _v.findViewById(R.id.check);
			final LinearLayout linear2 = (LinearLayout) _v.findViewById(R.id.linear2);
			final TextView title = (TextView) _v.findViewById(R.id.title);
			final TextView subtitle = (TextView) _v.findViewById(R.id.subtitle);
			
			title.setText(_data.get((int)_position).get("title").toString());
			if (_data.get((int)_position).containsKey("subtitle")) {
				subtitle.setText(_data.get((int)_position).get("subtitle").toString());
			}
			else {
				subtitle.setVisibility(View.GONE);
			}
			if (_data.get((int)_position).containsKey("check")) {
				check.setClickable(false);
				check.setChecked("true".equals(_data.get((int)_position).get("check").toString()));
			}
			else {
				check.setVisibility(View.GONE);
			}
			itemViews.add(_position, _v);
			
			return _v;
		}
	}
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input){
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels(){
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels(){
		return getResources().getDisplayMetrics().heightPixels;
	}
	
}
