package com.lhw.sketchware.codeeditor;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.net.*;
import android.text.*;
import android.util.*;
import android.webkit.*;
import android.animation.*;
import android.view.animation.*;
import java.util.*;
import java.text.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import java.util.ArrayList;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.Intent;
import android.net.Uri;
import java.util.Timer;
import java.util.TimerTask;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;

public class CodeeditActivity extends AppCompatActivity {
	
	private Timer _timer = new Timer();
	
	private Toolbar _toolbar;
	private double historyIndex = 0;
	private String projectName = "";
	private boolean isDirty = false;
	private boolean needHighlight = false;
	private double i = 0;
	private String findTarget = "";
	private double findIndex = 0;
	
	private ArrayList<String> history = new ArrayList<>();
	private ArrayList<Double> highlightPos = new ArrayList<>();
	
	private LinearLayout lay;
	private EditText edit;
	
	private SharedPreferences tuto;
	private Intent intent = new Intent();
	private SharedPreferences fileSetting;
	private TimerTask timer;
	private AlertDialog.Builder dlg;
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.codeedit);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		
		_toolbar = (Toolbar) findViewById(R.id._toolbar);
		setSupportActionBar(_toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _v) {
				onBackPressed();
			}
		});
		lay = (LinearLayout) findViewById(R.id.lay);
		edit = (EditText) findViewById(R.id.edit);
		tuto = getSharedPreferences("tutorial", Activity.MODE_PRIVATE);
		fileSetting = getSharedPreferences("setting", Activity.MODE_PRIVATE);
		dlg = new AlertDialog.Builder(this);
		
		edit.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence _param1, int _param2, int _param3, int _param4) {
				final String _charSeq = _param1.toString();
				isDirty = true;
				needHighlight = true;
				setTitle("*".concat(projectName));
				/*DEVELOPING
int start = _start;
int end = _start + _count;
boolean isPauseEditing = false;

int s = start;
int e = end;

while(isEquals(edit.getText().charAt(s), HIGHLIGHTSEARCHCUT) || s == 0) s--;
while(isEquals(edit.getText().charAt(e), HIGHLIGHTSEARCHCUT) || e == edit.getText().length() - 1) e++;
if(_count > 200){ edit.setEnabled(false); isPauseEditing = true; }

_highlight(s, e);
if(isPauseEditing) edit.setEnabled(true);
*/
				if(Integer.parseInt(fileSetting.getString("autoSaveInterval", "-1")) > 0)
				timer = new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								
							}
						});
					}
				};
				_timer.scheduleAtFixedRate(timer, (int)(Double.parseDouble(fileSetting.getString("autoSaveInterval", ""))), (int)(Double.parseDouble(fileSetting.getString("autoSaveInterval", ""))));
			}
			
			@Override
			public void beforeTextChanged(CharSequence _param1, int _param2, int _param3, int _param4) {
				
			}
			
			@Override
			public void afterTextChanged(Editable _param1) {
				
			}
		});
	}
	private void initializeLogic() {
		manager = new UndoManager();
		historyIndex = 0;
		needHighlight = false;
		projectName = getIntent().getStringExtra("project");
		edit.setTextSize(Integer.parseInt(fileSetting.getString("textSize", "16")));
		edit.setTypeface(Typeface.MONOSPACE);
		edit.addTextChangedListener(manager.textWatcher);
		
		_loadfile();
		isDirty = false;
		setTitle(projectName);
		if ("".equals(tuto.getString("t1", ""))) {
			SketchwareUtil.showMessage(getApplicationContext(), "Press meun button or press more button to see more features.");
			tuto.edit().putString("t1", "true").commit();
		}
	}
	public boolean isEquals(char tg, String texts){ for(int i=0; i<texts.length(); i++) if(texts.charAt(i) == tg) return true; return false; }
	public UndoManager manager;
	//TEXTS. USED WHEN HIGHLIGHT
	private String[][] texts;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		//when menu creates
		menu.add("Save").setIcon(R.drawable.ic_save_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add("Undo").setIcon(R.drawable.ic_undo_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menu.add("Redo").setIcon(R.drawable.ic_redo_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		menu.add("Find");
		menu.add("Replace");
		menu.add("Settings");
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		if(item.getItemId() == android.R.id.home) _exitFind();
		else _handleMenu(item.getTitle().toString());
		return true;
	}
	
	@Override
	protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
		super.onActivityResult(_requestCode, _resultCode, _data);
		
		switch (_requestCode) {
			
			default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		if(getActionBar().getDisplayOptions() == ActionBar.DISPLAY_SHOW_CUSTOM) _exitFind();
		else if(isDirty){
			dlg.setTitle("Save files");
			dlg.setMessage("Projects was not saved.\nDo you want to exit?");
			dlg.setPositiveButton("Save and Exit", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					_save();
					finish();
				}
			});
			dlg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					
				}
			});
			dlg.setNeutralButton("Exit", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					finish();
				}
			});
			dlg.create().show();
		} else finish();
	}
	private void _handleMenu (final String _item) {
		//Save,Undo,Redo,Find,Replace,and Settings
		if (_item.equals("Save")) {
			_save();
			SketchwareUtil.showMessage(getApplicationContext(), "Save completed!");
		}
		else {
			if (_item.equals("Undo")) {
				manager.undo(edit.getText());
			}
			else {
				if (_item.equals("Redo")) {
					manager.redo(edit.getText());
				}
				else {
					if (_item.equals("Find")) {
						Context c = getActionBar().getThemedContext();
						LinearLayout lay = new LinearLayout(c);
						EditText target = new EditText(c);
						ImageButton back = new ImageButton(c);
						back.setImageResource(R.drawable.ic_arrow_back_white);
						ImageButton next = new ImageButton(c);
						next.setImageResource(R.drawable.ic_arrow_forward_white);
						lay.addView(target);
						lay.addView(back);
						lay.addView(next);
						ActionBar a = getActionBar();
						a.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
						a.setCustomView(lay);
					}
					else {
						if (_item.equals("Replace")) {
							
						}
						else {
							if (_item.equals("Settings")) {
								intent.setAction(Intent.ACTION_VIEW);
								intent.setClass(getApplicationContext(), SettingActivity.class);
								startActivity(intent);
							}
							else {
								
							}
						}
					}
				}
			}
		}
	}
	
	
	private void _save () {
		timer.cancel();
		new Thread(){public void run(){
				try { java.io.FileWriter writer = new java.io.FileWriter(fileDir + "/files/" + projectName); writer.write(edit.getText().toString()); writer.flush(); writer.close(); CodeeditActivity.this.runOnUiThread(new Runnable(){public void run(){
							isDirty = false;
							setTitle(projectName);
						}});
				} catch(final java.io.IOException e){ CodeeditActivity.this.runOnUiThread(new Runnable(){ public void run(){new AlertDialog.Builder(CodeeditActivity.this).setTitle("Error happened during saving file :)").setPositiveButton("Okay", null).setNeutralButton("Show Details", new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int witch){ StringBuilder data = new StringBuilder(); for(int i=0; i<e.getStackTrace().length; i++)data.append(e.getStackTrace()[i].toString() + "\\n"); new AlertDialog.Builder(CodeeditActivity.this).setTitle("Error Details").setMessage(e.getMessage() + "\\n" + data.toString().substring(0, data.toString().length() - 2)).setPositiveButton("Okay", null).show(); }}).show(); }}); }
			}}.start();
	}
	
	
	private void _loadfile () {
		java.io.File file = new java.io.File(fileDir + "/files/" + projectName);
		if(!file.exists()) new AlertDialog.Builder(this).setTitle(projectName + " do not exist!").setPositiveButton("OKAY", new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int witch){ CodeeditActivity.this.finish(); }}).show();
		try {
			java.io.FileInputStream is = new java.io.FileInputStream(file);
			java.io.InputStreamReader reader = new java.io.InputStreamReader(is);
			char[] txt = new char[is.available()];
			for(int i=0; true; i++){
				  int t = reader.read();
				  if(t == -1) break;
				  txt[i] = (char) t;
				  
			}
			is.close();
			edit.setText(String.valueOf(txt));
		} catch(java.io.IOException e){ new AlertDialog.Builder(this).setTitle("Opening file failed").setMessage(e.getMessage()).setCancelable(false).setPositiveButton("OKAY", new DialogInterface.OnClickListener(){ public void onClick(DialogInterface dlg, int witch){ CodeeditActivity.this.finish(); }}).show(); }
	}
	
	
	private void _highlight (final double _start, final double _end) {
		
	}
	
	
	private void _exitFind () {
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE);
		getActionBar().setDisplayHomeAsUpEnabled(false);
	}
	
	
	private void _findNext (final double _increase) {
		i = findIndex;
		for(int _repeat12 = 0; _repeat12 < (int)((edit.getText().toString().length() - findTarget.length())); _repeat12++) {
			if (edit.getText().toString().substring((int)(i), (int)((i + findTarget.length()) - 1)).equals(findTarget)) {
				edit.setSelection((int)i, (int)i + findTarget.length() - 1);
				i = i + _increase;
				if (i > (edit.getText().toString().length() - 1)) {
					i = 0;
					SketchwareUtil.showMessage(getApplicationContext(), "return to first");
				}
				else {
					if (i < 0) {
						i = 0;
						SketchwareUtil.showMessage(getApplicationContext(), "reached first");
						break;
					}
				}
				break;
			}
		}
	}
	
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input){
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels(){
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels(){
		return getResources().getDisplayMetrics().heightPixels;
	}
	
}
