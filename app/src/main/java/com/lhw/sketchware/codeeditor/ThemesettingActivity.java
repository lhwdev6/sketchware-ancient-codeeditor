package com.lhw.sketchware.codeeditor;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.net.*;
import android.text.*;
import android.util.*;
import android.webkit.*;
import android.animation.*;
import android.view.animation.*;
import java.util.*;
import java.text.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.TextView;
import android.widget.HorizontalScrollView;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.View;

public class ThemesettingActivity extends AppCompatActivity {
	
	
	private Toolbar _toolbar;
	private boolean edited = false;
	private double iconCur = 0;
	
	private LinearLayout lay;
	private Button reset;
	private TextView textview1;
	private HorizontalScrollView hscroll1;
	private LinearLayout iconlay;
	
	private SharedPreferences fileSetting;
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.themesetting);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		
		_toolbar = (Toolbar) findViewById(R.id._toolbar);
		setSupportActionBar(_toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _v) {
				onBackPressed();
			}
		});
		lay = (LinearLayout) findViewById(R.id.lay);
		reset = (Button) findViewById(R.id.reset);
		textview1 = (TextView) findViewById(R.id.textview1);
		hscroll1 = (HorizontalScrollView) findViewById(R.id.hscroll1);
		iconlay = (LinearLayout) findViewById(R.id.iconlay);
		fileSetting = getSharedPreferences("setting", Activity.MODE_PRIVATE);
		
		reset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _view) {
				primaryPicker.setColor(Color.parseColor(DEFAULTCOLORPRIMARY));
				_save();
				SketchwareUtil.showMessage(getApplicationContext(), "Theme is currently reseted. Restart app to apply.");
				edited = false;
			}
		});
	}
	private void initializeLogic() {
		edited = false;
		iconCur = -1;
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle("Theme Setting");
		if(apiLevel >= 21){
			android.graphics.drawable.GradientDrawable g = new android.graphics.drawable.GradientDrawable();
			g.setColor(0x33f7f7f7);
			g.setCornerRadius(3);
			reset.setBackground(new android.graphics.drawable.RippleDrawable(android.content.res.ColorStateList.valueOf(0xff000000), g, null));
		}
		primaryPicker = new FoldableColorPicker(this);
		primaryPicker.setColor(Color.parseColor(fileSetting.getString("colorPrimary", "COLORPRIMARY NOT EXIST")));
		primaryPicker.setTitle("Theme color");
		primaryPicker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener(){
			public void onColorChanged(ColorPicker picker, int color){
				colorPrimary = color;
				edited = true;
				loadThemeColor();
			}
		});
		lay.addView(primaryPicker);
		for(int i =0; i < iconIds.length; i++){
			final int index = i;
			ImageView img = new ImageView(this);
			ViewGroup.MarginLayoutParams mlp = new ViewGroup.MarginLayoutParams((int) getDip(70), (int) getDip(70));
			mlp.setMargins(8, 8, 8, 8);
			img.setLayoutParams(mlp);
			img.setImageResource(iconIds[i]);
			img.setOnClickListener(new View.OnClickListener(){
				public void onClick(View v){
					_setIconSelection(index);
					edited = true;
				}
			});
			iconlay.addView(img);
		}
		int appIcon = Integer.parseInt(fileSetting.getString("appIcon", "FREFERENCE appIcon NOT EXIST"));
		int i;
		for(i=0; i<iconIds.length; i++) if(iconIds[i] == appIcon) break;
		Log.i("ICON", Integer.toString(i));
		if(i >= 0 && i < iconIds.length) _setIconSelection(i);
	}
	private FoldableColorPicker primaryPicker;
	private int[] iconIds = {
		R.drawable.thumb_main,
		R.drawable.thumb_gray,
		R.drawable.thumb_white,
		R.drawable.thumb_pink,
		R.drawable.thumb_orange,
		R.drawable.thumb_skyblue,
		R.drawable.thumb_bluegreen,
		R.drawable.thumb_indigo,
		R.drawable.thumb_black
	};
	 @Override public boolean onCreateOptionsMenu(Menu menu){
		menu.add("Save").setIcon(R.drawable.ic_save_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		if(item.getItemId() == android.R.id.home){ if(edited){ new AlertDialog.Builder(this).setTitle("Theme Setting").setMessage("Theme was not saved. Do you want to save curren edited theme?").setPositiveButton("Save", new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int witch){ _save(); showMessage("Theme was saved. Restart app to apply theme."); finish(); }}).setNegativeButton("Exit", new DialogInterface.OnClickListener(){ public void onClick(DialogInterface dlg, int witch){ finish(); }}).show(); } else finish(); }
		else if(item.getTitle() == "Save"){
			_save();
			showMessage("Theme was saved. Restart app to apply theme.");
		}
		return true;
	}
	
	@Override
	protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
		super.onActivityResult(_requestCode, _resultCode, _data);
		
		switch (_requestCode) {
			
			default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		if(edited){ new AlertDialog.Builder(this).setTitle("Theme Setting").setMessage("Theme was not saved. Do you want to save curren edited theme?").setPositiveButton("Save", new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int witch){ _save(); showMessage("Theme was saved. Restart app to apply theme"); finish(); }}).setNegativeButton("Exit", new DialogInterface.OnClickListener(){ public void onClick(DialogInterface dlg, int witch){ finish(); }}).show(); } else finish();
	}
	private void _save () {
		SharedPreferences.Editor edit = fileSetting.edit();
		edit.putString("colorPrimary", primaryPicker.getText());
		if(iconCur < iconIds.length) edit.putString("appIcon", Integer.toString(iconIds[(int) iconCur]));
		edit.apply();
		edited = false;
	}
	
	
	private void _setIconSelection (final double _tg) {
		if((int) iconCur != -1) ((ImageView) iconlay.getChildAt((int) iconCur)).setColorFilter(0x00000000);
		((ImageView) iconlay.getChildAt((int) _tg)).setColorFilter(0x55000000);
		iconCur = _tg;
	}
	
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input){
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels(){
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels(){
		return getResources().getDisplayMetrics().heightPixels;
	}
	
}
