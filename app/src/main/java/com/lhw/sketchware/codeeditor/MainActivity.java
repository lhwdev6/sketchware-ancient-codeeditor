package com.lhw.sketchware.codeeditor;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.net.*;
import android.text.*;
import android.util.*;
import android.webkit.*;
import android.animation.*;
import android.view.animation.*;
import java.util.*;
import java.text.*;
import android.support.v7.app.AppCompatActivity;
import java.util.HashMap;
import java.util.ArrayList;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;
import android.content.Intent;
import android.net.Uri;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.app.Activity;
import android.content.SharedPreferences;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ChildEventListener;

public class MainActivity extends AppCompatActivity {
	
	private Timer _timer = new Timer();
	private FirebaseDatabase _firebase = FirebaseDatabase.getInstance();
	
	private double temp = 0;
	private HashMap<String, Object> vCurData = new HashMap<>();
	private boolean isDebugEnabled = false;
	private boolean posted = false;
	
	private ArrayList<HashMap<String, Object>> versionDatas = new ArrayList<>();
	
	private LinearLayout linear;
	private ImageView image;
	private TextView text;
	
	private TimerTask timer;
	private Intent mainScreenIntent = new Intent();
	private AlertDialog.Builder dlg;
	private SharedPreferences fileSetting;
	private DatabaseReference firebase = _firebase.getReference("appdata/releases");
	private ChildEventListener _firebase_child_listener;
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.main);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		
		linear = (LinearLayout) findViewById(R.id.linear);
		image = (ImageView) findViewById(R.id.image);
		text = (TextView) findViewById(R.id.text);
		dlg = new AlertDialog.Builder(this);
		fileSetting = getSharedPreferences("setting", Activity.MODE_PRIVATE);
		
		_firebase_child_listener = new ChildEventListener() {
			@Override
			public void onChildAdded(DataSnapshot _param1, String _param2) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onChildChanged(DataSnapshot _param1, String _param2) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onChildMoved(DataSnapshot _param1, String _param2) {
				
			}
			
			@Override
			public void onChildRemoved(DataSnapshot _param1) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onCancelled(DatabaseError _param1) {
				final int _errorCode = _param1.getCode();
				final String _errorMessage = _param1.getMessage();
				
			}
		};
		firebase.addChildEventListener(_firebase_child_listener);
	}
	private void initializeLogic() {
		getActionBar().hide();
		linear.setBackgroundColor(Color.parseColor(fileSetting.getString("colorPrimary", "#ff0000")));
		text.setVisibility(View.GONE); if(fileSetting.getBoolean("debugEnabled", false)){ text.setText("Developer Mode"); text.setTextColor(0x55ffffff);
			text.setTextSize(14); text.setVisibility(View.VISIBLE);
			linear.getOverlay().add(new android.graphics.drawable.ColorDrawable(0x33666666)); }
		isDebugEnabled = fileSetting.getBoolean("debugEnabled", false);
		_init();
		timer = new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (!posted) {
							timer.cancel();
							_start();
						}
					}
				});
			}
		};
		_timer.schedule(timer, (int)(4000));
	}
	
	@Override
	protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
		super.onActivityResult(_requestCode, _resultCode, _data);
		
		switch (_requestCode) {
			
			default:
			break;
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		if(timer != null)
		timer.cancel();
		finish();
	}
	private void _first () {
		if(apiLevel < 21) new AlertDialog.Builder(this).setTitle("Warning").setMessage("Your device Android version is lower than 21.\\nCodeEditor was maded that can be worked on lower devices, but some features might can\\'t work.f bug happen, please send bug report in the error page.").show();
		langCodes = new String[]{"ko", "en"};
		text.setText("CodeEditor");
		text.setVisibility(View.VISIBLE);
		text.setTextColor(0xffffffff);
		text.setTextSize(20);
		text.setPadding(0, 0, 0, 52);
		LinearLayout lay = new LinearLayout(this);
		lay.setOrientation(LinearLayout.VERTICAL);
		ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		lp.setMargins(16, 32, 16, 32);
		lay.setLayoutParams(lp);
		lay.setPadding(8, 8, 8, 8);
		LinearLayout layIn = new LinearLayout(this);
		layIn.setPadding(16, 16, 16, 16);
		layIn.setOrientation(LinearLayout.VERTICAL);
		android.graphics.drawable.Drawable d = new android.graphics.drawable.Drawable(){
						@Override
						public void draw(Canvas c)
						{
								Rect b = getBounds();
								Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
								p.setColor(0xfff7f7f7);
								c.drawRoundRect(b.left, b.top, b.right, b.bottom, 3, 3, p);
						}
						@Override public void setAlpha(int p1){}
						@Override public void setColorFilter(ColorFilter p1){}
						@Override public int getOpacity(){ return -3; }
				};
		lay.setBackground(d);
		if(apiLevel >= 21) lay.setElevation(6);
		final Spinner sp = new Spinner(this);
		sp.setPadding(8, 8, 8, 8);
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item);
		for(String str : langCodes){
			adapter.add(new Locale(str).getDisplayLanguage());
		}
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp.setAdapter(adapter);
		layIn.addView(sp);
		LinearLayout layBar = new LinearLayout(this);
		layBar.setGravity(Gravity.RIGHT);
		layBar.setPadding(16, 8, 4, 0);
		Button b = new Button(this, null, android.R.attr.buttonBarPositiveButtonStyle);
		b.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		b.setText("Start");
		b.setOnClickListener(new View.OnClickListener(){ public void onClick(View v){
				//TODO: SET LANGUAGE
				String lang = "en";
				switch(sp.getSelectedItemPosition()){ case 0: lang = "en"; break; case 1: lang = "ko"; break; }
				fileSetting.edit().putString("lang", lang).putString("started", "true").commit();
				MainActivity.this.startActivity(new Intent(MainActivity.this, MainActivity.class));
				finish();
			}});
		layBar.addView(b);
		lay.addView(layIn);
		lay.addView(layBar);
		linear.addView(lay);
	}
	private String[] langCodes;
	void _temp_firstInner(){
	}
	
	
	private void _init () {
		if ("".equals(fileSetting.getString("textSize", ""))) {
			fileSetting.edit().putString("textSize", "12").commit();
		}
		if ("".equals(fileSetting.getString("isDarkTheme", ""))) {
			fileSetting.edit().putString("isDarkTheme", "false").commit();
		}
		if ("".equals(fileSetting.getString("autoSaveInterval", ""))) {
			fileSetting.edit().putString("autoSaveInterval", "60000").commit();
		}
		if ("".equals(fileSetting.getString("appIcon", ""))) {
			fileSetting.edit().putString("appIcon", Integer.toString(R.drawable.thumb_main)).apply();
		}
		image.setImageBitmap(mainIcon);
		firebase.addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot _dataSnapshot) {
				versionDatas = new ArrayList<>();
				try {
					GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
					for (DataSnapshot _data : _dataSnapshot.getChildren()) {
						HashMap<String, Object> _map = _data.getValue(_ind);
						versionDatas.add(_map);
					}
				}
				catch (Exception _e) {
					_e.printStackTrace();
				}
				if (versionDatas.size() == 0) {
					if (isDebugEnabled) {
						Toast.makeText(MainActivity.this, "Firebase DB is wrong", Toast.LENGTH_LONG).show();
					}
				}
				else {
					vCurData = versionDatas.get((int)versionDatas.size() - 1);
					bundle.putSerializable("versionDatas", versionDatas);
					if (isDebugEnabled) {
						Toast.makeText(MainActivity.this, "version: " + vCurData.get("versionName"), Toast.LENGTH_SHORT).show();
					}
				}
				posted = true;
				timer = new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								_start();
							}
						});
					}
				};
				_timer.schedule(timer, (int)(1500));
			}
			@Override
			public void onCancelled(DatabaseError _databaseError) {
			}
		});
	}
	
	
	private void _start () {
		/*if(fileSetting.getBoolean("havePass", false)){ switch(fileSetting.getString("passType", "")){
case "edit":
LinearLayout lay = new LinearLayout(this);
ViewGroup.MarginLayoutParams mlp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.FILL_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
mlp.setMargins(32, 32, 32, 32);
lay.setLayoutParams(mlp);
android.graphics.drawable.GradientDrawable d = new android.graphics.drawable.GradientDrawable();
d.setColor(0xffffffff);
d.setCornerRadius(3);
lay.setBackground(d);
if(apiLevel >= 21) lay.setElevation(3);
EditText edit = new EditText(this);
edit.setHint("password");
lay.addView(edit);
ImageButton btn = new ImageButton(this);
android.graphics.drawable.Drawable icon = getDrawable(R.drawable.ic_send_white);
icon.setColorFilter(0xff3388ff, PorterDuff.Mode.SRC_IN);
btn.setImageDrawable(icon);
if(apiLevel < 21){
android.graphics.drawable.ColorStateDrawable btnBg = new android.graphics.drawable.ColorStateDrawable();

android.graphics.drawable.GradientDrawable _d = new android.graphics.drawable.GradientDrawable();
_d.setColor(0xffffffff);
_d.setCornerRadii(new float[]{0, 0, 3, 3, 3, 3, 0, 0});
btnBg.addState(new int[]{android.R.attr.state_hovered}, _d);
}
else
btn.setBackground(new RippleDrawable(android.content.res.ColorStateList.valueOf(0x33000000), null, null));
lay.addView(btn);
*/
		if ("".equals(fileSetting.getString("started", ""))) {
			_first();
		}
		else {
			mainScreenIntent.setAction(Intent.ACTION_VIEW);
			mainScreenIntent.setClass(getApplicationContext(), ProjectslistActivity.class);
			startActivity(mainScreenIntent);
			timer = new TimerTask() {
				@Override
				public void run() {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							finish();
						}
					});
				}
			};
			_timer.schedule(timer, (int)(400));
		}
	}
	
	
	private void _source () {
	}}

/**
 * @author LHW
 * @version 1.0
 * Activity for data sharing and theme loading
 * Currently translation not developed
 */
class Activity extends android.app.Activity
{
		public static final String DEFAULTCOLORPRIMARY = "#37474f";
		public static final float DEFAULTSTATUSBARDARKNESS = 0.77f;
		
		private static final String TAG = "Activity";
		private static final boolean isLoadLangpack = false; //for debug
		
		public static int apiLevel = Build.VERSION.SDK_INT; //for api level simulation
		public static int version;
		public static String versionName;
		
		protected org.json.JSONObject translateData;
		protected org.json.JSONObject globalTranslateData;
		protected boolean isloading = true;
		protected boolean isStartTask = false;
		protected Runnable actionAfterLoad;
		protected boolean isUpdatedNow = false;
		protected int colorPrimary = Color.parseColor(DEFAULTCOLORPRIMARY);
		protected boolean isThemeSetting = true;
		protected Thread logcatThread;
		protected boolean strictMode = false;
		protected boolean unstrict = true;
		protected boolean sendLogcat = true;
		protected Bitmap mainIcon;
		protected String fileDir;
		protected Bundle bundle;
		
		private SharedPreferences fs;
		
		
		protected void onCreate(Bundle savedInstanceState){
				super.onCreate(savedInstanceState);
				
				
				try {
						fs = getSharedPreferences("setting", MODE_PRIVATE);
						if(fs.getBoolean("debugEnabled", false)){
								//debug
								if(sendLogcat) startLogCat();
								strictMode = true;
								if(!fs.contains("strictMode")) fs.edit().putBoolean("strictMode", true).apply();
						}
						strictMode = fs.getBoolean("strictMode", strictMode);
						if(!fs.contains("colorPrimary")) fs.edit().putString("colorPrimary", DEFAULTCOLORPRIMARY).commit();
						
						Log.d("Activity", "started Activity");
						
						//init
						setTheme((fs.contains("theme")? fs.getInt("theme", -1) : (apiLevel < 21? android.R.style.Theme_Holo_Light_DarkActionBar : android.R.style.Theme_Material_Light_DarkActionBar)));
						if(isThemeSetting){
								colorPrimary = Color.parseColor(fs.getString("colorPrimary", "COLORPRIMARY NOT EXIST"));
								loadThemeColor();
						}
						if(getIntent().hasExtra("startData")){
								this.bundle = getIntent().getBundleExtra("startData");
						} else this.bundle = new Bundle();
						
						//loading
						android.content.pm.PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
						version = info.versionCode;
						versionName = info.versionName;
			
						if(fs.contains("version") && version == fs.getInt("version", 0)){
								//not updated now
								if(new Random().nextInt(70) == 12){
										//easter egg
										bundle.putBoolean("easterEgg", true);
										runOnUiThread(new Runnable(){
													public void run(){
															Toast.makeText(Activity.this, "easter egg randomly worked!", Toast.LENGTH_LONG).show();
													}
											});
								}
								if(bundle.getBoolean("easterEgg", false)){
										//TODO EASTER EGG
										runOnUiThread(new Runnable(){
													public void run(){
							
													}
											});
								}
				
						} else {
								//updated now
								isUpdatedNow = true;
								fs.edit().putInt("version", version).apply();
						}
			if(!fs.contains("appIcon")) fs.edit().putString("appIcon", Integer.toString(R.drawable.thumb_main)).commit();
						mainIcon = BitmapFactory.decodeResource(getResources(), Integer.parseInt(fs.getString("appIcon", "appIcon NOT EXIST")));
						
						if(apiLevel >= 21){
								ActivityManager.TaskDescription desc = new ActivityManager.TaskDescription(Const.APPNAME, mainIcon, colorPrimary);
				
								setTaskDescription(desc);
						}
						if(isLoadLangpack){
								if(!new java.io.File(getFilesDir() + "/langpack.json").exists() || isUpdatedNow){
										unpackLangPack("en"); //TODO: VARIOUS LANGUAGES
								}
								if(getIntent().hasExtra("translateData")) translateData = (org.json.JSONObject) getIntent().getSerializableExtra("translateData");
								else loadLangPack();
						}
						isloading = false;
				} catch(Exception e){
						if(unstrict){
								e.printStackTrace();
								new AlertDialog.Builder(Activity.this)
									.setTitle("Error")
									.setMessage(e.getMessage())
									.setPositiveButton("Okay", null)
									.show();
						}
						else throw new RuntimeException(e);
				}
				if(fs.getBoolean("storagePrivate", false)) fileDir = getDataDir().getPath();
				else fileDir = getExternalFilesDir(null).getPath();
		}
			
		
		protected void unpackLangPack(final String lang)
		{
				Log.d("CodeEditor", "unpacking language pack: " + lang);
				
				try {
						java.util.zip.ZipInputStream is = new java.util.zip.ZipInputStream(getResources().openRawResource(/*TODO: RAW FILE NAME(eg: R.raw.langpack)*/0));
						java.util.zip.ZipEntry entry;
						while((!((entry = is.getNextEntry()) == null)) && entry.getName() != "lang_" + lang + ".json"){}
						if(entry == null){
								if(strictMode) throw new RuntimeException("no langpack " + lang + " is exist");
								Log.e(TAG, "cannot load langpack");
								return;
						}
						
						java.io.FileOutputStream os = new java.io.FileOutputStream(getFilesDir() + "/langpack.json");
						byte[] b = new byte[255];
						int size;
						while((size = is.read(b)) > 0) os.write(b, 0, size);
						is.close();
						os.flush();
						os.close();
				} catch(Exception e){
						e.printStackTrace();
						throw new RuntimeException(e);
				}
		}
		
		protected void loadLangPack()
		{
				Log.d("CodeEditor", "loading language pack");
				
				try {
						java.io.FileInputStream is = new java.io.FileInputStream(getFilesDir() + "/langpack.json");
						java.io.InputStreamReader reader = new java.io.InputStreamReader(is);
						char[] texts = new char[is.available() / 2];
						reader.read(texts);
						reader.close();
						org.json.JSONArray obj = new org.json.JSONArray(String.valueOf(texts));
						org.json.JSONObject transObj = obj.getJSONObject(1);
						
						try {
								//asserts
								if(transObj.optString("program") != "CodeEditor"
									|| false //...
								) throw new RuntimeException("wrong translation file");
								
								//check version
								int objVersion = transObj.getInt("versionBuild");
								if(objVersion < version){
										//necessary text might not exist
										Log.e(TAG, "old translation file");
										if(strictMode) throw new RuntimeException("Translation file version is old.");
										Toast.makeText(this, "Translation file version is old.", Toast.LENGTH_SHORT).show();
								} else if(objVersion > version){
										Log.w(TAG, "newer translation file");
								}
								
						}
						catch(org.json.JSONException e){}
						catch(Exception e){
								throw new RuntimeException(e);
						}
						org.json.JSONObject datas = transObj.getJSONObject("data");
						translateData = datas.getJSONObject(this.getClass().getName());
						globalTranslateData = datas.getJSONObject("global");
				} catch(Exception e){
						throw new RuntimeException(e);
				}
		}
		
		public String t(String name)
		{
				try {
						return translateData.getString(name);
				} catch(Exception e){
						if(strictMode) throw new RuntimeException("Translate error: " + name + " is not exist!", e);
						else {
								
								return "null";
						}
				}
		}
		
		protected void startLogCat()
		{
				if(logcatThread != null) return;
				logcatThread = new Thread("LogCat"){
						public void run(){
								try {
										java.lang.Process process = Runtime.getRuntime().exec("logcat -v threadtime");
										java.io.InputStreamReader inputStreamReader = new java.io.InputStreamReader(process.getInputStream());
										java.io.BufferedReader bufferedReader = new java.io.BufferedReader((java.io.Reader)inputStreamReader, 20);
										do {
												String text;
						           	    		if ((text = bufferedReader.readLine()) == null) {
														return;
							                	}
						                		//send logcat lines
												Intent intent = new Intent();
												intent.setPackage("com.aide.ui");
												intent.setAction("com.adrt.LOGCAT_ENTRIES");
												intent.putExtra("lines", new String[]{text});
												Activity.this.sendBroadcast(intent);
						            	} while (true);
					        	}
				        		catch (java.io.IOException e) {
					            		return;
					        	}
						}
				};
				logcatThread.setPriority(2);
				logcatThread.start();
		}
		
		public void startActivity(Intent intent)
		{
				intent.putExtra("startData", bundle);
				intent.putExtra("translateData", (java.io.Serializable) translateData);
				super.startActivity(intent);
		}
		
		public int getColorPrimaryDark()
		{
				return Color.rgb((int)(Color.red(colorPrimary) * DEFAULTSTATUSBARDARKNESS), (int)(Color.green(colorPrimary) * DEFAULTSTATUSBARDARKNESS), (int)(Color.blue(colorPrimary) * DEFAULTSTATUSBARDARKNESS));
		}
		
		protected void loadThemeColor()
		{
				if(apiLevel >= 21) getWindow().setStatusBarColor(getColorPrimaryDark());
				getActionBar().setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(colorPrimary));
		}
}


 class Action {
			public Action(){}
			public Action(int pos, byte action, CharSequence text){
					this.pos = pos;
					this.action = action;
					this.text = text;
			}
			public Action(int pos, byte action, CharSequence text, CharSequence text2){
					this.pos = pos;
					this.action = action;
					this.text = text;
					this.text2 = text2;
			}
			
			public byte action;
			public CharSequence text;
			public CharSequence text2;
			public int pos;
			
			public static final byte ACTION_INSERT = 0;
			public static final byte ACTION_REPLACE = 1;
			public static final byte ACTION_DELETE = 2;
	}
 class UndoManager
{
		/** by LHW
    * https://gusdn2.github.io/
    */
		public ArrayList<Action> history = new ArrayList<Action>();
		public int historyIndex = -1;
		public boolean actioning = false;
		
		public TextWatcher textWatcher = new TextWatcher(){
				public CharSequence lastText;
				@Override
				public void beforeTextChanged(CharSequence txt, int start, int before, int count)
				{
						if(actioning) return;
						lastText = txt.subSequence(start, start + before);
				}
				@Override
				public void onTextChanged(CharSequence txt, int start, int before, int count)
				{
						if(actioning) return;
						if(before == 0 && count > 0) history.add(new Action(start, Action.ACTION_INSERT, txt.subSequence(start, start + count)));
						else if(before >= 0 && count > 0) history.add(new Action(start, Action.ACTION_REPLACE, lastText, txt.subSequence(start, start + count)));
						else history.add(new Action(start, Action.ACTION_DELETE, lastText));
						if(history.size() > 30) history.remove(0);
						if(history.size() == historyIndex + 1) historyIndex++;
						else historyIndex = history.size() - 1;
				}
				@Override
				public void afterTextChanged(Editable edit)
				{
						
				}
		};
		public void undo(Editable e)
		{
				if(historyIndex <= -1) return;
				Action cur = history.get(historyIndex);
				actioning = true;
				switch(cur.action){
						case Action.ACTION_INSERT: e.delete(cur.action + cur.pos, cur.pos + cur.text.length()); break;
						case Action.ACTION_REPLACE: e.replace(cur.pos, cur.pos + cur.text2.length(), cur.text); break;
						case Action.ACTION_DELETE: e.insert(cur.pos, cur.text); break;
				}
				actioning = false;
				historyIndex--;
		}
		public void redo(Editable e)
		{
				if(historyIndex >= history.size() - 1 || historyIndex == -1) return;
				historyIndex++;
				Action cur = history.get(historyIndex);
				actioning = true;
				switch(cur.action){
						case Action.ACTION_INSERT: e.insert(cur.pos, cur.text); break;
						case Action.ACTION_DELETE: e.delete(cur.pos, cur.pos + cur.text.length()); break;
						case Action.ACTION_REPLACE: e.replace(cur.pos, cur.pos + cur.text.length(), cur.text2); break;
				}
				actioning = false;
		}
}
class ColorPicker extends LinearLayout
{
		public static interface OnColorChangedListener
		{
				public void onColorChanged(ColorPicker picker, int color);
		}
		
		
		private SeekBar r;
		private SeekBar g;
		private SeekBar b;
		private EditText hex;
		private TextView colorShow;
		private SeekBar.OnSeekBarChangeListener listener;
		private OnColorChangedListener l;
		
		
		public ColorPicker(Context c)
		{
				super(c);
				init();
		}
		
		private void init(){
				setPadding(16, 16, 16, 16);
				setGravity(Gravity.CENTER);
				setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				colorShow = new TextView(getContext());
				colorShow.setLayoutParams(new ViewGroup.LayoutParams(60, 60));
				addView(colorShow);
				
				listener = new SeekBar.OnSeekBarChangeListener(){
						@Override
						public void onProgressChanged(SeekBar p1, int p2, boolean p3)
						{
								int color = Color.rgb(r.getProgress(), g.getProgress(), b.getProgress());
								String temp = String.format("%1$08x", color);
								String result = temp.substring(2);
								hex.setText("#" + result);
								hex.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);
								colorShow.setBackgroundColor(color);
								
								if(l != null) l.onColorChanged(ColorPicker.this, color);
						}
						@Override public void onStartTrackingTouch(SeekBar p1){}
						@Override public void onStopTrackingTouch(SeekBar p1){}
				};
				
				LinearLayout lay2 = new LinearLayout(getContext());
				lay2.setOrientation(VERTICAL);
				lay2.setPadding(8, 8, 8, 8);
				lay2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				hex = new EditText(getContext());
				ViewGroup.MarginLayoutParams params = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				params.setMargins(16, 0, 16, 0);
				hex.setLayoutParams(params);
				hex.setImeOptions(android.view.inputmethod.EditorInfo.IME_ACTION_DONE);
				hex.setText("#000000"); //PUT CURRENT THEME COLOR
				hex.setOnEditorActionListener(new TextView.OnEditorActionListener(){
							@Override
							public boolean onEditorAction(TextView text, int code, KeyEvent event)
							{
									try {
											int color = Color.parseColor(text.getText().toString());
											r.setProgress(Color.red(color));
											g.setProgress(Color.green(color));
											b.setProgress(Color.blue(color));
									} catch(Exception e){
											Toast.makeText(getContext(), "Color code is wrong", Toast.LENGTH_SHORT).show();
									}
									return true;
							}
					});
				lay2.addView(hex);
				
				r = new SeekBar(getContext());
				setProgressColor(r, 0xffcc5577);
				r.setMax(255);
				r.setOnSeekBarChangeListener(listener);
				lay2.addView(r);
				
				g = new SeekBar(getContext());
				setProgressColor(g, 0xff339977);
				g.setMax(255);
				g.setOnSeekBarChangeListener(listener);
				lay2.addView(g);
				
				b = new SeekBar(getContext());
				setProgressColor(b, 0xff6077bb);
				b.setMax(255);
				b.setOnSeekBarChangeListener(listener);
				lay2.addView(b);
				addView(lay2);
				
				int color = Color.parseColor(hex.getText().toString());
				r.setProgress(Color.red(color));
				g.setProgress(Color.green(color));
				b.setProgress(Color.blue(color));
				colorShow.setBackgroundColor(color);
		}
		public String getText(){ listener.onProgressChanged(null, 0, false); return hex.getText().toString(); }
		public void setColor(int color)
		{
				hex.setText("#" + String.format("%1\$08x", color).substring(2));
				r.setProgress(Color.red(color));
				g.setProgress(Color.green(color));
				b.setProgress(Color.blue(color));
		}
		
		public int getColor(boolean refreshFromSlider)
		{
				if(refreshFromSlider) listener.onProgressChanged(null, 0, false);
				return Color.parseColor(hex.getText().toString());
		}
		
		public int getColor()
		{
				return getColor(true);
		}
		
		public void setOnColorChangedListener(OnColorChangedListener l)
		{
				this.l = l;
		}
		
		private void setProgressColor(AbsSeekBar bar, int color)
		{
				bar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
				if(Activity.apiLevel >= 16) bar.getThumb().setColorFilter(color, PorterDuff.Mode.SRC_IN);
		}
}
class FoldableColorPicker extends LinearLayout implements ColorPicker.OnColorChangedListener
{
		private TextView text;
		private ImageButton button;
		private ColorPicker picker;
		private Bitmap down, up;
		private boolean opened = false;
		private ColorPicker.OnColorChangedListener l;
		private View.OnClickListener bl;
		
		
		public FoldableColorPicker(Context context)
		{
				super(context);
				
				setOrientation(VERTICAL);
				setPadding(16, 16, 16, 16);
				
				bl = new View.OnClickListener(){
						@Override
						public void onClick(View view)
						{
								opened = !opened;
								updateButton();
						}
				};
				
				LinearLayout bar = new LinearLayout(getContext());
				button = new ImageButton(getContext());
				button.setBackground(new android.graphics.drawable.RippleDrawable(android.content.res.ColorStateList.valueOf(0x33000000), null, null));
				button.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
				button.setOnClickListener(bl);
				bar.setOnClickListener(bl);
				bar.addView(button);
				
				text = new TextView(getContext());
				text.setTextSize(14);
				bar.addView(text);
				
				addView(bar);
				
				picker = new ColorPicker(getContext());
				picker.setPadding(0, 0, 0, 0);
				down = BitmapFactory.decodeResource(getResources(), Const.ic_arrow_down_black);
				up = BitmapFactory.decodeResource(getResources(), Const.ic_arrow_up_black);
				updateButton();
				addView(picker);
				
				picker.setOnColorChangedListener(this);
		}
		
		public void setTitle(String text)
		{
				this.text.setText(text);
		}
		
		public void setColor(int color)
		{
				picker.setColor(color);
		}
		
		public int getColor()
		{
				return picker.getColor();
		}
		
		public int getColor(boolean refreshFromSlider)
		{
				return picker.getColor(refreshFromSlider);
		}
		
		public void setOnColorChangedListener(ColorPicker.OnColorChangedListener l)
		{
				this.l = l;
		}
		public String getText(){ return picker.getText(); }
		/** @hide */
		@Override
		public void onColorChanged(ColorPicker picker, int color)
		{
				text.setTextColor(color);
				if(l != null) l.onColorChanged(picker, color);
		}
		
		private void updateButton()
		{
				if(opened){
						button.setImageBitmap(up);
						picker.setVisibility(VISIBLE);
				} else {
						button.setImageBitmap(down);
						picker.setVisibility(GONE);
				}
		}
}


class __TempClass_AvoidBug_Activity extends android.app.Activity
{
	void _temp(){
		final class Const
		{
				public static final String APPNAME = "CodeEditor";
				public static int MAINICONID = 0;
				public static final int ic_arrow_down_black = R.drawable.ic_arrow_drop_down_black;
				public static final int ic_arrow_up_black = R.drawable.ic_arrow_drop_up_black;
		}
	}
	
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input){
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels(){
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels(){
		return getResources().getDisplayMetrics().heightPixels;
	}
	
}
