package com.lhw.sketchware.codeeditor;

import android.app.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.net.*;
import android.text.*;
import android.util.*;
import android.webkit.*;
import android.animation.*;
import android.view.animation.*;
import java.util.*;
import java.text.*;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import java.util.HashMap;
import java.util.ArrayList;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.content.Intent;
import android.net.Uri;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.app.Activity;
import android.content.SharedPreferences;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ChildEventListener;

public class ProjectslistActivity extends AppCompatActivity {
	
	private FirebaseDatabase _firebase = FirebaseDatabase.getInstance();
	
	private Toolbar _toolbar;
	private String versionName = "";
	private double versionCode = 0;
	private HashMap<String, Object> vCurData = new HashMap<>();
	private HashMap<String, Object> vLatestData = new HashMap<>();
	
	private ArrayList<String> projects = new ArrayList<>();
	private ArrayList<HashMap<String, Object>> versionDatas = new ArrayList<>();
	
	private ListView list;
	
	private Intent intent = new Intent();
	private AlertDialog.Builder dlg;
	private SharedPreferences tuto;
	private SharedPreferences fileSetting;
	private DatabaseReference firebase = _firebase.getReference("appdata");
	private ChildEventListener _firebase_child_listener;
	@Override
	protected void onCreate(Bundle _savedInstanceState) {
		super.onCreate(_savedInstanceState);
		setContentView(R.layout.projectslist);
		initialize(_savedInstanceState);
		initializeLogic();
	}
	
	private void initialize(Bundle _savedInstanceState) {
		
		_toolbar = (Toolbar) findViewById(R.id._toolbar);
		setSupportActionBar(_toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View _v) {
				onBackPressed();
			}
		});
		list = (ListView) findViewById(R.id.list);
		dlg = new AlertDialog.Builder(this);
		tuto = getSharedPreferences("tutorial", Activity.MODE_PRIVATE);
		fileSetting = getSharedPreferences("setting", Activity.MODE_PRIVATE);
		
		_firebase_child_listener = new ChildEventListener() {
			@Override
			public void onChildAdded(DataSnapshot _param1, String _param2) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onChildChanged(DataSnapshot _param1, String _param2) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onChildMoved(DataSnapshot _param1, String _param2) {
				
			}
			
			@Override
			public void onChildRemoved(DataSnapshot _param1) {
				GenericTypeIndicator<HashMap<String, Object>> _ind = new GenericTypeIndicator<HashMap<String, Object>>() {};
				final String _childKey = _param1.getKey();
				final HashMap<String, Object> _childValue = _param1.getValue(_ind);
				
			}
			
			@Override
			public void onCancelled(DatabaseError _param1) {
				final int _errorCode = _param1.getCode();
				final String _errorMessage = _param1.getMessage();
				
			}
		};
		firebase.addChildEventListener(_firebase_child_listener);
	}
	private void initializeLogic() {
		setTitle("My Projects");
		java.io.File f = new java.io.File(fileDir + "/files/"); if(!f.exists()) f.mkdirs();
		list = new ListView(this);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
			public void onItemClick(AdapterView a, View v, int p, long id){ _onListviewClicked(p); }});
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
			public boolean onItemLongClick(AdapterView a, View v, int p, long id){ _onListviewLongClick(p); return true; }});
		_checkVersion();
		_loadList();
	}
	
	@Override public boolean onCreateOptionsMenu(Menu menu)
	{
		   menu.add("New").setIcon(R.drawable.ic_add_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add("Settings").setIcon(R.drawable.ic_settings_white).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		   return true;
	}
	
	@Override public boolean onMenuItemSelected(int featureId, MenuItem item){
		   _handleMenu(item.getTitle().toString());
		   return true;
	}
	
	@Override
	protected void onActivityResult(int _requestCode, int _resultCode, Intent _data) {
		super.onActivityResult(_requestCode, _resultCode, _data);
		
		switch (_requestCode) {
			
			default:
			break;
		}
	}
	
	private void _loadList () {
		projects.clear();
		String[] files = new java.io.File(fileDir + "/files/").list();
		if(files == null || files.length == 0){ View v = LayoutInflater.from(this).inflate(R.layout.whenempty, null); v.setLayoutParams(new ViewGroup.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)); setContentView(v); Button b = (Button) findViewById(R.id.button);
			b.setBackground(null);
			b.setOnClickListener(new View.OnClickListener(){public void onClick(View view){ _handleMenu("New"); }}); return; } else { setContentView(list); }
		for(String f : files) projects.add(f);
		list.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, projects));
	}
	
	
	private void _handleMenu (final String _menu) {
		if (_menu.equals("New")) {
			final EditText edit = new EditText(this); edit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
			edit.setHint("project name");
			final LinearLayout view = new LinearLayout(this);
			view.addView(edit);
			view.setPadding(45, 30, 45, 30);
			new AlertDialog.Builder(this).setTitle("New Project").setView(view).setPositiveButton("Okay",new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int w){ _newProject(edit.getText().toString()); }}).setNegativeButton("Cancel",null).show();
		}
		else {
			if (_menu.equals("Settings")) {
				intent.setAction(Intent.ACTION_VIEW);
				intent.setClass(getApplicationContext(), SettingActivity.class);
				startActivity(intent);
			}
			else {
				
			}
		}
	}
	
	
	private void _newProject (final String _name) {
		if(_name.matches("(/|!|projects)")){
			dlg.setTitle("Name is wrong!");
			dlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					
				}
			});
			dlg.create().show();
		} else if(new java.io.File(fileDir + "/files/" + _name).exists()){
			dlg.setTitle(_name.concat(" is already exists!"));
			dlg.setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					
				}
			});
			dlg.create().show();
		} else {
			try{ if(!new java.io.File(fileDir + "/files/" + _name).createNewFile()){ showMessage("making failed!"); return; }}catch(Exception e){showMessage("making failed!" + e.getMessage());return;}
			projects.add(_name);
			intent.setAction(Intent.ACTION_VIEW);
			intent.putExtra("project", _name);
			intent.setClass(getApplicationContext(), CodeeditActivity.class);
			startActivity(intent);
			_loadList();
		}
	}
	
	
	private void _handleAction (final double _pos, final double _index) {
		//when long click the listview and select option: Open,Rename,Delete
		if (_index == 0) {
			intent.setAction(Intent.ACTION_VIEW);
			intent.putExtra("project", projects.get((int)(_pos)));
			intent.setClass(getApplicationContext(), CodeeditActivity.class);
			startActivity(intent);
		}
		else {
			if (_index == 1) {
				final LinearLayout lay  = new LinearLayout(this);
				final EditText _edit = new EditText(this);
				_edit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				_edit.setHint(projects.get((int)_pos));
				lay.addView(_edit);
				lay.setPadding(45, 30, 45, 30);
				new AlertDialog.Builder(this)
				.setTitle("Rename " + projects.get((int)_pos))
				.setView(lay)
				.setPositiveButton("OKAY", new DialogInterface.OnClickListener(){
					   public void onClick(DialogInterface dlg, int witch){ if(_edit.getText().toString() == "") _edit.setText(projects.get((int)_pos));
						//rename
						java.io.File file = new java.io.File(fileDir + "/files/" + projects.get((int)_pos));
						if(file.renameTo(new java.io.File(fileDir + "/files/" + _edit.getText().toString()))){
							showMessage("rename completed");
							_loadList(); } else { new AlertDialog.Builder(ProjectslistActivity.this).setTitle("Error").setMessage("Rename failed").setPositiveButton("Okay", null).show(); }}})
				.setNegativeButton("CANCEL", null)
				.show();
			}
			else {
				if (_index == 2) {
					dlg.setTitle("Delete Project");
					dlg.setMessage("Do you sure to delete ".concat(projects.get((int)(_pos)).concat("?\nYou can't undo after delete file!")));
					dlg.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface _dialog, int _which) {
							new java.io.File(fileDir + "/files/" + projects.get((int)_pos)).delete();
							projects.remove((int)_pos);
							_loadList();
						}
					});
					dlg.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface _dialog, int _which) {
							
						}
					});
					dlg.create().show();
				}
				else {
					
				}
			}
		}
	}
	
	
	private void _onListviewClicked (final double _index) {
		intent.setAction(Intent.ACTION_VIEW);
		intent.putExtra("project", projects.get((int)(_index)));
		intent.setClass(getApplicationContext(), CodeeditActivity.class);
		startActivity(intent);
	}
	
	
	private void _onListviewLongClick (final double _index) {
		final int pos = (int)_index;
		new AlertDialog.Builder(ProjectslistActivity.this).setTitle(projects.get(pos)).setItems(new CharSequence[]{"Open","Rename","Delete"},new DialogInterface.OnClickListener(){public void onClick(DialogInterface dlg, int index){_handleAction(pos, index);}}).show();
	}
	
	
	private void _checkVersion () {
		versionDatas = (ArrayList<HashMap<String,Object>>) bundle.getSerializable("versionDatas");
		if(versionDatas == null || versionDatas.size() == 0){
			if(strictMode) throw new RuntimeException("versionDatas not exist");
			else {
				showMessage("versionDatas not exist");
				return;
			}
		}
		vLatestData = versionDatas.get(versionDatas.size() - 1);
		vCurData = versionDatas.get(version - 1);
		if((long) vCurData.get("version") != version) throw new RuntimeException("unknown error: FIREBASE DB IS WRONG; appdata/release/v" + version + "/version data is not match");
		if ("".equals(tuto.getString("versionCheck", ""))) {
			versionName = super.versionName; versionCode = super.version;
			dlg.setTitle("Version ".concat(versionName));
			dlg.setMessage(vCurData.get("updateLogs").toString());
			dlg.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface _dialog, int _which) {
					
				}
			});
			dlg.create().show();
			tuto.edit().putString("versionCheck", "true").commit();
		}
		if ("".equals(tuto.getString("updateCheck", ""))) {
			if((long) vCurData.get("version") > version){
				//update available
				dlg.setTitle("Update available");
				dlg.setMessage("version ".concat(vLatestData.get("versionName").toString().concat("\n".concat(vLatestData.get("updateLogs").toString()))));
				dlg.setPositiveButton("Update", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface _dialog, int _which) {
						//UPDATE
						showMessage("update!");
					}
				});
				dlg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface _dialog, int _which) {
						tuto.edit().putString("updateCheck", "checked").commit();
					}
				});
				dlg.create().show();
			}
		}
	}
	
	
	@Deprecated
	public void showMessage(String _s) {
		Toast.makeText(getApplicationContext(), _s, Toast.LENGTH_SHORT).show();
	}
	
	@Deprecated
	public int getLocationX(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[0];
	}
	
	@Deprecated
	public int getLocationY(View _v) {
		int _location[] = new int[2];
		_v.getLocationInWindow(_location);
		return _location[1];
	}
	
	@Deprecated
	public int getRandom(int _min, int _max) {
		Random random = new Random();
		return random.nextInt(_max - _min + 1) + _min;
	}
	
	@Deprecated
	public ArrayList<Double> getCheckedItemPositionsToArray(ListView _list) {
		ArrayList<Double> _result = new ArrayList<Double>();
		SparseBooleanArray _arr = _list.getCheckedItemPositions();
		for (int _iIdx = 0; _iIdx < _arr.size(); _iIdx++) {
			if (_arr.valueAt(_iIdx))
			_result.add((double)_arr.keyAt(_iIdx));
		}
		return _result;
	}
	
	@Deprecated
	public float getDip(int _input){
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, _input, getResources().getDisplayMetrics());
	}
	
	@Deprecated
	public int getDisplayWidthPixels(){
		return getResources().getDisplayMetrics().widthPixels;
	}
	
	@Deprecated
	public int getDisplayHeightPixels(){
		return getResources().getDisplayMetrics().heightPixels;
	}
	
}
